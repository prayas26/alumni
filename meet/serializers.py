from rest_framework import serializers
from .models import Alumnus, Question, Answer, Suggest, Change, GENDER_CHOICE, BRANCH_CHOICE


class AlumnusSerializer(serializers.ModelSerializer):
	class Meta:
		model = Alumnus
		fields = (
			'enrollment_no',
			'name',
			'gender',
			'dob',
			'contactno',
			'image',
			'fblink',
			'linkedin',
			'email',
			'branch',
			'batch',
			)

class QuestionSerializer(serializers.ModelSerializer):
	class Meta:
		model = Question
		fields = ('question_text',)

class AnswerSerializer(serializers.ModelSerializer):
	class Meta:
		model = Answer
		fields = (
			'question',
			'answer',
			)
		
class SuggestSerializer(serializers.ModelSerializer):
	class Meta:
		model = Suggest
		fields=(
			'sname',
			'semail',
			'smessage',
			)

class ChangeSerializer(serializers.ModelSerializer):
<<<<<<< HEAD
    class Meta:
        model = Change
        field=(
			'cname',
			'cemail',
			'emessage'
		)
=======
	class Meta:
		fields=(
		    'cname',
		    'cbatch',
		    'cemail',
		    'ccontact',
		    'cdetail',
		)
    
>>>>>>> 64a72570f36b6768b1092918e6f0fc54311f2e09
