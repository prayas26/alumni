from django.db import models
import datetime
from django.core.validators import MaxValueValidator, MinValueValidator

now = datetime.datetime.now()

GENDER_CHOICE = (
	('M', 'Male'),
	('F', 'Female'),
)

BRANCH_CHOICE = (
	('BTCS', 'B.Tech Computer Science and Technology'),
)


class Alumnus(models.Model):
	enrollment_no = models.CharField('Enrollment No.', max_length=12, primary_key=True, default='AXXXXXXXXXXX')
	name = models.CharField(max_length=50)
	gender = models.CharField(default='Gender', max_length=1, choices=GENDER_CHOICE)
	dob = models.DateField('Date of Birth', default = 'DD/MM/YYYY')
<<<<<<< HEAD
	contactno = models.IntegerField('Contact No.')
=======
	contactno = models.IntegerField('Contact No.', validators=[MaxValueValidator(10), MinValueValidator(10)])
>>>>>>> 64a72570f36b6768b1092918e6f0fc54311f2e09
	image = models.ImageField('Add Image')
	fblink = models.CharField('Facebook ID Link : http://www.facebook.com/', max_length=100)
	linkedin = models.CharField('LinkedIn Link : http://www.linkedin.com/', max_length=100)
	email = models.EmailField('Email ID',)
	branch = models.CharField(choices=BRANCH_CHOICE, max_length=20)
	batch = models.IntegerField('Passout Year', default='%d' %now.year)

	class Meta:
		ordering = ('name', 'batch')

	def __unicode__(self):
		return self.name

class Question(models.Model):
	question_text = models.CharField(max_length=50)

	def __unicode__(self1):
		return self1.question_text

class Answer(models.Model):
	question = models.ForeignKey(Question)
	answer = models.CharField(max_length=100)

class Suggest(models.Model):
	sname = models.CharField('Name', max_length=50)
	semail = models.EmailField('Email ID')
	smessage = models.TextField('Message', max_length=250)
	
	class Meta:
		ordering = ('semail', 'smessage')
	
	def __unicode__(self2):
		return self2.sname
	
class Change(models.Model):
<<<<<<< HEAD
    cname = models.CharField('Name', max_length=75)
    cemail = models.EmailField('Email ID', max_length=100)
    cmessage = models.TextField('Message', max_length=500)
	
    class Meta:
        ordering = ('cname', 'cemail')
		
    def __unicode__(self3):
    	return self3.cname
=======
    cname = models.CharField('Name', max_length=50)
    cbatch = models.IntegerField('Passout Year')
    cemail = models.EmailField('Email ID')
    ccontact = models.IntegerField('Contact No.', validators=[MaxValueValidator(10), MinValueValidator(10)])
    cdetail = models.TextField('Changes', max_length=500)
	
    class Meta:
    	ordering = ('cname', 'cemail')
		
    def __unicode__(self3):
    	return self3.cemail
>>>>>>> 64a72570f36b6768b1092918e6f0fc54311f2e09
