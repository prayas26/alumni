from rest_framework.routers import SimpleRouter
from meet import views


router = SimpleRouter()

router.register(r'alumnus', views.AlumnusViewSet)
router.register(r'question', views.QuestionViewSet)
router.register(r'suggestion', views.SuggestViewSet)
router.register(r'change', views.ChangeViewSet)

urlpatterns = router.urls