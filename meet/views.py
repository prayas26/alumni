from rest_framework.viewsets import ModelViewSet
from meet.serializers import AlumnusSerializer, QuestionSerializer, AnswerSerializer, SuggestSerializer, ChangeSerializer
from meet.models import Alumnus, Question, Answer, Suggest, Change, GENDER_CHOICE, BRANCH_CHOICE


class AlumnusViewSet(ModelViewSet):
    queryset = Alumnus.objects.all()
    serializer_class = AlumnusSerializer

class QuestionViewSet(ModelViewSet):
    queryset = Question.objects.all()
    serializer_class = QuestionSerializer

class AnswerViewSet(ModelViewSet):
    queryset = Answer.objects.all()
    serializer_class = AnswerSerializer
    
class SuggestViewSet(ModelViewSet):
    queryset = Suggest.objects.all()
    serializer_class = SuggestSerializer
    
class ChangeViewSet(ModelViewSet):
    queryset = Change.objects.all()
    serializer_class = ChangeSerializer