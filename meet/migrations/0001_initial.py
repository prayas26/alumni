# -*- coding: utf-8 -*-
# Generated by Django 1.9.2 on 2016-02-21 20:15
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Alumnus',
            fields=[
                ('enrollment_no', models.CharField(default=b'AXXXXXXXXXXX', max_length=12, primary_key=True, serialize=False, verbose_name=b'Enrollment No.')),
                ('title', models.CharField(choices=[(b'MR', b'Mr.'), (b'MRS', b'Mrs.'), (b'MS', b'Ms.')], default=b'Title', max_length=3)),
                ('name', models.CharField(max_length=50)),
                ('gender', models.CharField(choices=[(b'M', b'Male'), (b'F', b'Female')], default=b'Gender', max_length=1)),
                ('dob', models.DateField(default=b'01/01/1990', verbose_name=b'Date of Birth')),
                ('contactno', models.IntegerField(verbose_name=b'Contact No.')),
                ('fblink', models.URLField(max_length=100, verbose_name=b'Facebook ID Link')),
                ('linkedin', models.URLField(max_length=100, verbose_name=b'LinkedIn Link')),
                ('email', models.EmailField(max_length=254, verbose_name=b'Email ID')),
                ('batch', models.IntegerField(default=b'2016', verbose_name=b'Paasout Year')),
            ],
            options={
                'ordering': ('name', 'batch'),
            },
        ),
        migrations.CreateModel(
            name='Answer',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('answer', models.CharField(max_length=100)),
            ],
        ),
        migrations.CreateModel(
            name='Question',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('question_text', models.CharField(max_length=50)),
            ],
        ),
        migrations.AddField(
            model_name='answer',
            name='question',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='meet.Question'),
        ),
    ]
