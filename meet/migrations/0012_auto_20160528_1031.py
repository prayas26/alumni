# -*- coding: utf-8 -*-
# Generated by Django 1.9.6 on 2016-05-28 05:01
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('meet', '0011_change'),
    ]

    operations = [
        migrations.AlterField(
            model_name='alumnus',
            name='contactno',
            field=models.IntegerField(verbose_name=b'Contact No.'),
        ),
    ]
