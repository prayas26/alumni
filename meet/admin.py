from django.contrib import admin

from .models import Alumnus, Question, Answer, Suggest, Change

class AlumnusAdmin(admin.ModelAdmin):
    list_display = ["enrollment_no", "batch", "name", "contactno", "email"]
    list_filter = ["batch"]
    search_fields = ["enrollment_no","contactno+"]
    fields = [
    	'enrollment_no',
    	'name',
    	'gender',
    	'dob',
    	'contactno',
        'image',
    	'email',
    	'fblink',
    	'linkedin',
        'branch',
    	'batch',
    ]
    readonly_fields = ('batch',)

class AnswerInLine(admin.TabularInline):
	model = Answer
	extra = 3
	# fields = ['alumnus', 'question', 'answer']

class QuestionAdmin(admin.ModelAdmin):
	fields = ['question_text',]
	inlines = [AnswerInLine]
    
class SuggestAdmin(admin.ModelAdmin):
    fields = [
        'sname',
        'semail',
        'smesage',
    ]
    
class ChangeAdmin(admin.ModelAdmin):
    fields = [
        'cname',
        'cemail',
        'cmessage',
    ]

class SuggestAdmin(admin.ModelAdmin):
    model = Suggest
    fields = [
        'sname',
        'semail',
        'smessage',
    ]

class ChangeAdmin(admin.ModelAdmin):
    model = Change
    fields = [
        'cname',
        'cbatch',
        'cemail',
        'ccontact',
        'cdetail',
    ]

admin.site.register(Alumnus, AlumnusAdmin)
admin.site.register(Question, QuestionAdmin)
<<<<<<< HEAD
# admin.site.register(Answer, AnswerAdmin)
=======
>>>>>>> 64a72570f36b6768b1092918e6f0fc54311f2e09
admin.site.register(Suggest, SuggestAdmin)
admin.site.register(Change, ChangeAdmin)