from django.conf.urls import include, url
from django.contrib import admin

urlpatterns = [
    url(r'^', include('meet.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'', include('two_factor.urls', 'two_factor')),
]